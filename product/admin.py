from django.contrib import admin
from .models import Category, Product, Comment, Render, PointComment

# Register your models here.
admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Comment)
admin.site.register(Render)
admin.site.register(PointComment)


