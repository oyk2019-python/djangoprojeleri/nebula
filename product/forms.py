from django import forms
from product.models import Category,Render,Product, Comment, PointComment

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['category_name']


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name','price', 'category', 'producer','description', 'image']
        widgets={
            'price':forms.NumberInput,
            
        }

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text','image','url']
        
class RenderForm(forms.ModelForm):
    class Meta:
        model = Render
        fields = ['text','image','url']

class PointForm(forms.ModelForm):
    class Meta:
        model = PointComment
        fields = ['point']

    def __init__(self, *args, **kwargs):
        self.comment = kwargs.pop("comment")
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        obj = super().save(commit=False)
        obj.comment = self.comment
        obj.save(commit)
        return obj





    