# Generated by Django 2.2.3 on 2019-07-28 07:06

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0005_auto_20190728_0703'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='likes_count',
        ),
        migrations.RemoveField(
            model_name='render',
            name='likes_count',
        ),
        migrations.AddField(
            model_name='comment',
            name='upvote',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(5)]),
        ),
    ]
