# Generated by Django 2.2.3 on 2019-07-28 07:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0008_auto_20190728_0717'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pointcomment',
            old_name='upvote',
            new_name='point',
        ),
    ]
