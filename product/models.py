from django.db import models
from django.conf import settings
from PIL import Image
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.

class Category(models.Model):
    category_name = models.CharField(max_length = 30, null = True, blank=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.category_name

    class Meta:
        verbose_name = 'Kategori'
        verbose_name_plural = 'Kategoriler'



class Product(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    price = models.CharField(max_length = 7)
    producer = models.CharField(max_length = 50)
    image =models.ImageField(null=True,blank=True, upload_to="img")
    name = models.CharField(max_length=60, unique=True)
    category = models.ForeignKey('Category', on_delete = models.CASCADE)
    description = models.TextField(max_length = 200, null=True,blank=True)


    

    def __str__(self):
        return self.name

    def get_cat_list(self):
        return self.category

    class Meta:
        verbose_name = 'Ürün'
        verbose_name_plural = 'Ürünler'
    
     

class Comment(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)
    product = models.ForeignKey('Product', on_delete=models.CASCADE,related_name='comments')
    date = models.DateTimeField(auto_now= True)
    text = models.TextField(max_length=500)
    image = models.ImageField(null=True,blank=True, upload_to="img")
    url = models.URLField(max_length=200, null = True, blank=True)
    

    class Meta:
        verbose_name = 'Yorum'
        verbose_name_plural = 'Yorumlar'


    def __str__(self):
        return self.text
   

class Render(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)
    commentrender = models.ForeignKey('Comment', on_delete=models.CASCADE)
    product = models.ForeignKey('Product', on_delete=models.CASCADE)
    text = models.TextField(max_length=500)
    date = models.DateTimeField(auto_now= True)
    image = models.ImageField(null=True,blank=True)
    url = models.URLField(max_length=200, null = True, blank=True)

    
    def __str__(self):
        return '{} yorumuna {} cevabı verildi'.format(self.commentrender , self.text )


    class Meta:
        verbose_name = 'Cevap'
        verbose_name_plural = 'Cevaplar'

class PointComment(models.Model):
    comment = models.ForeignKey('Comment', on_delete=models.CASCADE)
    point = models.IntegerField(default = 0,validators=[MinValueValidator(0), MaxValueValidator(5)])

    class Meta:
        verbose_name = 'Yoruma Puan'
        verbose_name_plural = 'Yorumun Puanları'

    def __str__(self):
        return '{} yorumuna {} puan verildi'.format(self.comment , self.point )






        



    
        
    

