from django.contrib import admin
from django.urls import path,include
from .views import CategoryCreate, ProductCreate,ProductListView,CategoryProductView,CommentDetail,comment_to_product, RenderCreate, PointCreate

urlpatterns = [
#path('category', new_category, name = 'category'),
path('category', CategoryCreate.as_view(), name = 'category'),
path('addproduct', ProductCreate.as_view(), name = 'addproduct'),
path('productlist', ProductListView.as_view(), name = "productlist" ),
path('category_product/<int:pk>',CategoryProductView.as_view(),name='category_view'),
path('product/<int:pk>',CommentDetail.as_view(),name='product_detail'),
path('productd/<int:pk>',comment_to_product, name = 'product_comment'),
path('render', RenderCreate.as_view(), name = 'render_to_comment'),
path('point/<int:comment_id>', PointCreate.as_view(), name = "point" ),
]