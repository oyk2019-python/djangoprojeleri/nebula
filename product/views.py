from product.forms import CategoryForm, ProductForm, CommentForm, RenderForm, PointForm
from django.shortcuts import render
from django.urls import reverse_lazy
from .models import Category, Product, Comment, Render, PointComment
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
#def new_category(request):
#    form = CategoryForm()
#    return render(request,'product/product.html', {'form':form})


class CategoryCreate(LoginRequiredMixin,CreateView):
    model = Category
    form_class= CategoryForm
    login_url = '/admin'
    success_url = 'addproduct'
    template_name = 'product/product.html'

    def form_valid(self,form):
        form.instance.owner=self.request.user
        return super(CategoryCreate,self).form_valid(form)


class ProductCreate(LoginRequiredMixin,CreateView):
    model = Product
    form_class = ProductForm
    template_name = 'product/product_create.html'
    #context_object_name = 'as'
    login_url = '/admin'
    success_url = 'productlist'

    def form_valid(self,form):
        form.instance.owner=self.request.user
        return super(ProductCreate,self).form_valid(form)


from django.views.generic.list import ListView
from django.views import generic

class ProductListView(generic.ListView):
    model = Product
    #queryset = Product.objects.all()
    template_name = 'product/category_list.html'
    context_object_name = 'list'
    
    
class CategoryProductView(ListView):
    model=Product
    context_object_name = 'products'
    template_name = 'product/kategori_listele.html'

    def get_queryset(self):
        
        queryset = self.model.objects.filter(category_id=self.kwargs['pk'])
        return queryset

class CommentDetail(DetailView):
    model=Product
    context_object_name = 'product'
    template_name = 'product/yorum_listele.html'


    def get_context_data(self, **kwargs):
        context = super(CommentDetail,self).get_context_data(**kwargs)
        context["comments"] =self.object.comments.all()

        return context


from django.http import HttpResponse
   
def comment_to_product(request,pk):
    product=Product.objects.get(pk=pk)
    form=CommentForm()
    if request.method=='POST':
        form=CommentForm(request.POST, request.FILES)
        if form.is_valid():
            form.instance.owner=request.user
            form.instance.product=product
            
            form.save(True)
            return render(request,'product/back.html',{'product':product,'form':form})

    return render(request,'product/comment_create.html',{'product':product,'form':form})

from django.http import HttpResponse
   

class RenderCreate(LoginRequiredMixin,CreateView):
    model = Render
    form_class= RenderForm
    login_url = '/admin'
    success_url = 'productlist'
    template_name = 'product/render_create.html'

    def form_valid(self,form):
        form.instance.owner=self.request.user
    
        return super(RenderCreate,self).form_valid(form)

class PointCreate(LoginRequiredMixin,CreateView):
    model = PointComment
    form_class = PointForm
    login_url = '/admin'
    success_url = reverse_lazy('productlist')
    template_name = 'product/point_create.html'


    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["comment"] = Comment.objects.get(pk=self.kwargs["comment_id"])
        return kwargs

        


