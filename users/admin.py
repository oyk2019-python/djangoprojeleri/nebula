from django.contrib.auth.admin import UserAdmin
from .models import UserTable
from django.contrib import admin
# Register your models here.

#@admin.register(UserTable)
#class UserTable(UserAdmin):
#   fieldsets=UserAdmin.fieldsets + (
#              ('User Fields', {'fields':('telephone','image', 'first_name','last_name')})
#   )

@admin.register(UserTable)
class UserAdmin(admin.ModelAdmin):
    fields = ('first_name', 'last_name', 'telephone', 'image')
    #readonly_fields = ('', '')
    list_display = ('username', 'telephone')
