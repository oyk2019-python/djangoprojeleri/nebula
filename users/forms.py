from django import forms
from .models import UserTable
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.admin import UserAdmin
from django.forms import ModelForm

class RegisterForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = UserTable
        fields=[
            'first_name','last_name', 'image','telephone'
        ]

    def clean(self):
        username = self.cleaned_data.get("username")
        email= self.cleaned_data.get("email")
        first_name = self.cleaned_data.get("first_name")
        last_name = self.cleaned_data.get("last_name")
        password = self.cleaned_data.get("password")
        password2 = self.cleaned_data.get("password2")

        if (password and password2) and (password!=password2):
            raise forms.ValidationError('Passwords does not match!')
        elif UserTable.objects.filter(username=username).exists():
                raise forms.ValidationError('This username exist in system. Please try different username.')
        elif UserTable.objects.filter(email=email).exists():
                raise forms.ValidationError('This email exist in system. Please try different email')
        else:

            values = {
                'username':username,'password':password,'password2':password2,
                'first_name':first_name,'last_name':last_name
            }
            return values



class NewUserForm(UserCreationForm, forms.ModelForm):
    email = forms.EmailField(help_text = 'Email adresini girin')
    class Meta(UserCreationForm.Meta):
        model = UserTable
        fields = UserCreationForm.Meta.fields + ('email',"first_name", "last_name", "telephone","image")


class UserDetailForm(forms.ModelForm):
    class Meta:
        model = UserTable
        fields = ['first_name','last_name','image','telephone']


