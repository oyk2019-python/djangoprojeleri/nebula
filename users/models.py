from django.db import models
from django.contrib.auth.models import User, AbstractUser
from PIL import Image
from django.db import IntegrityError
# Create your models here.

class UserTable(AbstractUser):
    first_name = models.CharField(max_length = 30)
    last_name = models.CharField(max_length = 30)
    image =models.ImageField(null=True,blank=True)
    telephone = models.CharField(max_length = 20, blank = True, null = True)
    

