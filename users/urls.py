from django.contrib import admin
from django.urls import path,include
from .views import register, UserCreate
from .views import UserDetail, MeDetail, UserLoginView

urlpatterns = [
    path('register',UserCreate.as_view(),name='register'),
    path('detail/<pk>', UserDetail.as_view(), name = 'detail'),
    path('detailme/me/', MeDetail.as_view(), name = 'detailme'),
    path('sign',UserLoginView.as_view(), name = 'sign'),
  

]
   

