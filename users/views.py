from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from.forms import RegisterForm
from django.http import HttpResponse,HttpResponseRedirect
from .models import UserTable
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import reverse,redirect
from django.contrib.auth import login
from django.views.generic.edit import CreateView,UpdateView
from django.contrib.auth.forms import UserCreationForm
from django.db import IntegrityError
from django.contrib.auth.forms import AuthenticationForm
from users.forms import UserDetailForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
def register(request):

    form=RegisterForm(request.POST or None)
    if form.is_valid():
        username=form.cleaned_data.get('username')
        password=form.cleaned_data.get('password')
        first_name=form.cleaned_data.get('first_name')
        last_name=form.cleaned_data.get('last_name')
        email=form.cleaned_data.get('email')
        user=UserTable.objects.create_user(username=username,email=email,password=password,first_name=first_name,last_name=last_name)
        login(request,user)

        return HttpResponse('Basarili kayit')
    return render(request,'users/extend_sign.html',{'form':form})



    #class RegisterFormCBS(CreateView):
    #   model = UserTable
    #   form_class = RegisterForm
    #   success_url = '/admin'
    #   template_name = 'users/sign.html'
from .forms import NewUserForm
class UserCreate(CreateView):
    model = UserTable
    form_class=NewUserForm
    success_url = '/login'
    template_name = 'users/extend_sign.html'

from django.views.generic.detail import DetailView
from django.contrib.auth.views import LoginView,LogoutView

def sign_in(request):
    if request.method=='POST':
        form=AuthenticationForm(data=request.POST)
        if form.is_valid():
            return redirect('detail.html')
    else:
        form=AuthenticationForm()
    return render(request,'users/extend_sign.html',{'form':form})


    
class UserDetail(DetailView):
    model = UserTable
    form_class = UserDetailForm
    template_name = 'users/detail.html'
    #context_object_name = 'as'

class MeDetail(DetailView):
    model = UserTable
    template_name = 'users/detail.html'
    #context_object_name = 'as'

    def get_object(self, queryset = None):
        return self.request.user

class UserLoginView(LoginView):
    model = UserTable
    template_name = 'users/extend_sign.html'
    success_url = '/users/detail.html'



